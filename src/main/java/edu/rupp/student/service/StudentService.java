package edu.rupp.student.service;

import edu.rupp.student.dao.StudentDao;
import edu.rupp.student.dto.StudentDto;

public class StudentService {

	private StudentDao dao = new StudentDao();

	public boolean add(StudentDto dto) {
		return dao.add(dto);
	}

	public StudentDto get(String id) {
		return dao.get(id);
	}

	public boolean update(StudentDto dto) {
		return dao.update(dto);
	}

	public boolean remove(String id) {
		return dao.remove(id);
	}
}
