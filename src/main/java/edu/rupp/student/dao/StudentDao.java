package edu.rupp.student.dao;

import edu.rupp.student.dto.StudentDto;
import edu.rupp.student.util.TableStudent;

public class StudentDao {

	private TableStudent db = new TableStudent();

	public boolean add(StudentDto dto) {
		return db.insert(dto.getId(), dto.getName());
	}

	public StudentDto get(String id) {
		return db.select(id);
	}

	public boolean update(StudentDto dto) {
		return db.update(dto.getId(), dto.getName());
	}

	public boolean remove(String id) {
		return db.delete(id);
	}
}
