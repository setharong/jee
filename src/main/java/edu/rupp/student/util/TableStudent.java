package edu.rupp.student.util;

import java.util.HashMap;
import java.util.Map;

import edu.rupp.student.dto.StudentDto;

public class TableStudent {

	private Map<String, StudentDto> stuMap = new HashMap<String, StudentDto>();

	public TableStudent() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean insert(String id, String name) {
		return stuMap.putIfAbsent(id, new StudentDto(id, name)) == null;
	}

	public StudentDto select(String id) {
		return stuMap.get(id);
	}

	public boolean update(String id, String name) {
		try {
			stuMap.get(id).setName(name);
			return true;
		} catch (NullPointerException e) {
			// TODO: handle exception
			return false;
		}
	}

	public boolean delete(String id) {
		return stuMap.remove(id) != null;
	}
}
